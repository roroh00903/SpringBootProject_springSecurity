package com.unli.project.security;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.unli.project.domain.User;

import lombok.Data;

@Data
public class UserAuthentication implements UserDetails, Authentication {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2129031704493641096L;

	/** 인증 확인 여부 */
	private boolean authenticated;
	/** 유저 정보 */
	private User user;
	
	/** 인증 정보 - 아이디 */
	private String principal;
	/** 인증 정보 - 비밀번호 */
	private String credential;
	/** 인증 정보 - 토큰 */
	private String authToken;
	
	/* ---------------------------------- AdminAuthentication ---------------------------------- */
	
	/** authenticate with id/password */
	public UserAuthentication(String principal, String credential) {
		this.principal = principal;
		this.credential = credential;
	}
	
	/** authenticate with user info */
	public UserAuthentication(User user, boolean authenticated) {
		if(user != null) {
			setAuthenticated(authenticated);
			setUser(user);
		}
	}
	
	public void setUser(User user) {
		if(user != null) {
			this.user = user;
		}
	}
	
	/* ---------------------------------- UserDetails Overrides ---------------------------------- */
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}
	
	@Override
	public String getUsername() {
		return user.getUsername();
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return !user.isInvalid();
	}
	
	@Override
	public boolean isAccountNonLocked() {
		return !user.isAccountLocked();
	}
	
	@Override
	public boolean isCredentialsNonExpired() {
		return !user.isInvalid();
	}
	
	@Override
	public boolean isEnabled() {
		return !user.isInvalid();
	}
	
	/* ---------------------------------- Authentication Overrides ---------------------------------- */

	@Override
	public String getName() {
		if(user != null) return user.getName();
		else return null;
	}
	
	@Override
	public String getCredentials() {
		return this.credential;
	}
	
	@Override
	public UserAuthentication getDetails() {
		return this;
	}
	
	@Override
	public String getPrincipal() {
		return this.principal;
	}
	
	@Override
	public boolean isAuthenticated() {
		return this.authenticated;
	}
	
	@Override
	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
		this.authenticated = isAuthenticated;
	}
}
