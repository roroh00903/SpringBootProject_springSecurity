package com.unli.project.security;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.unli.project.domain.User;
import com.unli.project.repository.UserRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
	
	private UserRepository userRepo;
	
	private static final UsernameNotFoundException EXCEPTION = new UsernameNotFoundException("대상 유저를 찾을 수 없습니다.");
	
	@Override
	public UserAuthentication loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepo.findByUsernameAndInvalidFalse(username);
		if(user == null) {
			throw EXCEPTION;
		}
		return new UserAuthentication(user, false);
	}
}
