package com.unli.project.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.filter.GenericFilterBean;

import com.unli.project.constant.Constant;
import com.unli.project.domain.User;
import com.unli.project.service.UserLoginService;
import com.unli.project.util.StringUtil;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class StatelessAuthenticationFilter extends GenericFilterBean {
	
	private UserLoginService loginService;
	private CustomAuthenticationProvider authProvider;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException 
 {
		String userToken = ((HttpServletRequest) request).getHeader(Constant.USER_LOGIN_TOKEN_HEADER_NAME);
		
		if(StringUtil.isNullOrEmpty(userToken)) {
			chain.doFilter(request, response);
			return;
		}

		User user = loginService.getUserByToken(userToken, false, true);
		
		// authenticate
		UserAuthentication auth = new UserAuthentication(user, true);
		auth.setAuthToken(userToken);
		authProvider.authenticate(auth);
		
		// always continue
		chain.doFilter(request, response);

	}
}
