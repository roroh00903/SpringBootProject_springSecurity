package com.unli.project.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.unli.project.constant.Constant;
import com.unli.project.domain.User;
import com.unli.project.domain.UserAccessToken;
import com.unli.project.domain.UserLoginHistory;
import com.unli.project.exception.AccessTokenExpiredException;
import com.unli.project.exception.AccessTokenInvalidException;
import com.unli.project.repository.UserAccessTokenRepository;
import com.unli.project.repository.UserLoginHistoryRepository;
import com.unli.project.repository.UserRepository;
import com.unli.project.type.LoginFailureReason;
import com.unli.project.util.StringUtil;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserLoginService {

	private UserLoginHistoryRepository historyRepo;
	private UserRepository userRepo;
	private UserAccessTokenRepository tokenRepo;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
//	@Autowired
//	private MailService mailService;

	/** 로그인 성공 처리 */
	public UserLoginHistory loginSuccess(User user, String ip, String device, boolean resetCounter) {
		// 기록 생성
		UserLoginHistory history = buildBasicHistory(user, ip, device);
		history.setSuccess(true);
		historyRepo.save(history);

		// reset login fail counter
		if (resetCounter) {
			user.setLoginFailCount(0);
			user.setTemporaryPassword(false);
			userRepo.save(user);
		}

		return history;
	}

	/** 로그인 실패 처리 */
	public User loginFailed(String username, String ip, String device, AuthenticationException exception) {
		// find user by name
		if (StringUtil.isNullOrEmpty(username))
			return null;
		User user = userRepo.findByUsernameAndInvalidFalse(username);
		if (user == null)
			return null;

		// save history
		UserLoginHistory history = buildBasicHistory(user, ip, device);
		history.setSuccess(false);
		history.setFailureReason(LoginFailureReason.by(exception));
		historyRepo.save(history);

		// increase user login fail count
		int userFailCount = user.getLoginFailCount();
		userFailCount++;
		user.setLoginFailCount(userFailCount);

		userRepo.save(user);

		return user;
	}

	/**
	 * 토큰을 통해 유저 정보를 불러오기
	 * 
	 * @param token   쿠키에 저장 된 토큰
	 * @param refresh 만료일 갱신 여부
	 * @return 해당하는 유저 정보... 또는 null
	 */
	public User getUserByToken(String token, boolean refresh, boolean throwException) {
		if (StringUtil.isNullOrEmpty(token)) {
			if (throwException)
				throw AccessTokenInvalidException.getInstance();
			else
				return null;
		}

		// 토큰 정보 불러오기
		List<UserAccessToken> tokens = tokenRepo.findWithUserByTokenAndInvalidFalse(token);
		List<UserAccessToken> items = tokenRepo.findByToken(token);

		if (tokens.isEmpty()) {
			if (!items.isEmpty()) {
				items.get(0).setInvalid(false);
				items.get(0).setInvalidAt(null);
				return tokenRepo.save(refreshToken(items.get(0))).getUser();
			} else
				return new User();
		}

		// 유효 토큰 검사
		LocalDateTime now = LocalDateTime.now();
		UserAccessToken targetToken = null;
		for (UserAccessToken t : tokens) {
			// 이미 다른 토큰이 존재할 경우 invalid
			if (targetToken != null) {
				t.setInvalid(true);
				continue;
			}
			// 기간이 만료되었을 경우 invalid
			if (now.isAfter(t.getExpireAt())) {
				t.setInvalid(true);
			} else {
				targetToken = t;
			}
		}
		if (targetToken == null) {
			tokenRepo.saveAll(tokens);
			if (throwException)
				throw AccessTokenExpiredException.getInstance();
			else
				return null;
		}

		// 유저정보 입력
		User user = targetToken.getUser();
		if (user == null || user.isInvalid()) {
			targetToken.setInvalid(true);
			tokenRepo.save(targetToken);

			if (throwException)
				throw AccessTokenInvalidException.getInstance();
			else
				return null;
		}

		// 토큰 만료일 초기화
		if (refresh) {
			refreshToken(targetToken);
		}

		return user;
	}

	public void refreshToken(String tokenString) {
		if (tokenString == null)
			return;
		List<UserAccessToken> token = tokenRepo.findByTokenAndInvalidFalse(tokenString);
		if (!token.isEmpty())
			refreshToken(token.get(0));
	}

	public UserAccessToken refreshToken(UserAccessToken token) {
		if (token == null)
			return null;
		token.setExpireAt(calculateExpiryDate());
		return tokenRepo.save(token);
	}

	/**
	 * 대상 유저를 위한 로그인 토큰 생성
	 * 
	 * @param user 대상 유저
	 * @return token 값
	 */
	public String createToken(User user) {
		if (user == null)
			return "";
		String token = StringUtil.generateRandomString(24);

		UserAccessToken loginToken = new UserAccessToken();
		loginToken.setUser(user);
		loginToken.setToken(token);
		loginToken.setInvalid(false);
		loginToken.setExpireAt(calculateExpiryDate());
		tokenRepo.save(loginToken);

		return token;
	}

	/** 토큰 무효화 */
	public void invalidateToken(String token) {
		if (StringUtil.isNullOrEmpty(token))
			return;

		// 토큰 정보 불러오기
		List<UserAccessToken> tokens = tokenRepo.findByTokenAndInvalidFalse(token);
		invalidateToken(tokens);
	}

	/** 토큰 무효화 */
	public void invalidateToken(User user) {
		if (user == null)
			return;

		// 토큰 정보 불러오기
		List<UserAccessToken> tokens = tokenRepo.findByUserAndInvalidFalse(user);
		invalidateToken(tokens);
	}

	/** 토큰 무효화 유틸리티 */
	private void invalidateToken(List<UserAccessToken> tokens) {
		if (tokens == null || tokens.isEmpty())
			return;

		// 무효화 값 입력 및 저장
		for (UserAccessToken t : tokens) {
			t.setInvalid(true);
		}
		tokenRepo.saveAll(tokens);
	}

	/** 만료일 계산 */
	private LocalDateTime calculateExpiryDate() {
		LocalDateTime now = LocalDateTime.now();
		return now.plusDays(Constant.ADMIN_LOGIN_TOKEN_LIFESPAN);
	}

	/*
	 * -------------------------------------------- utility
	 * --------------------------------------------
	 */

	/** 로그인 기록 기본 값 */
	private UserLoginHistory buildBasicHistory(User user, String ip, String device) {
		// build for login history
		UserLoginHistory history = new UserLoginHistory();
		history.setUser(user);
		history.setIp(ip);
		history.setDevice(device);

		return history;
	}

	public User resetPassword(User user, boolean sendEmail) {
		String temporaryPassword = UUID.randomUUID().toString().replaceAll("-", "");
		temporaryPassword = temporaryPassword.substring(0, 8);

		user.setPassword(passwordEncoder.encode(temporaryPassword));
		System.out.println(temporaryPassword);
		if (sendEmail)
//			mailService.sendTemporaryPassword(user, temporaryPassword);
		user.setTemporaryPassword(true);

		return userRepo.save(user);
	}
}
