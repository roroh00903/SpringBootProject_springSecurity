package com.unli.project.dto;

import com.unli.project.domain.UserAccessToken;
import com.unli.project.security.UserAuthentication;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserAuthForUserDto {
	private UserDto user;
	private String authToken;

	
	public UserAuthForUserDto(UserAuthentication auth) {
		if(auth.getUser() != null) user = new UserDto(auth.getUser());
		authToken = auth.getAuthToken();
	}


	public UserAuthForUserDto(UserAccessToken userAccessToken) {
		this.user = new UserDto(userAccessToken.getUser());
		this.authToken = userAccessToken.getToken();
	}
}
