package com.unli.project.exception.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unli.project.dto.io.CommonResponse;

public class CustomAccessDeniedHandler implements AccessDeniedHandler {
	
	private static final CommonResponse<Void> ERROR_RESPONSE = new CommonResponse<>("403", "접근 권한이 없습니다.", null);
	
	@Override
	@SuppressWarnings("deprecation")
	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException accessDeniedException) throws IOException, ServletException {
		response.addHeader("content-type", MediaType.APPLICATION_JSON_UTF8_VALUE);
		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		
		response.getWriter().write(new ObjectMapper().writeValueAsString(ERROR_RESPONSE));
	}

}
