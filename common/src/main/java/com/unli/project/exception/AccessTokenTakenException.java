package com.unli.project.exception;

import org.springframework.http.HttpStatus;

import com.unli.project.dto.io.CommonResponse;

@SuppressWarnings("serial")
public class AccessTokenTakenException extends BaseException {
	
	private static final HttpStatus STATUS = HttpStatus.UNAUTHORIZED;
	private static final String MESSAGE = "계정과 일치 하지 않는 접근 토큰입니다.";
	private static final CommonResponse<Void> RESPONSE = new CommonResponse<>(String.valueOf(STATUS.value()), MESSAGE, null);
	private static final AccessTokenTakenException INSTANCE = new AccessTokenTakenException();
	private AccessTokenTakenException() {}
	
	@Override
	public HttpStatus getStatus() {
		return STATUS;
	}
	@Override
	public String getErrorCode() {
		return String.valueOf(STATUS.value());
	}
	@Override
	public String getErrorMessage() {
		return MESSAGE;
	}
	@Override
	public CommonResponse<Void> getErrorResponse() {
		return RESPONSE;
	}
	public static BaseException getInstance() {
		return INSTANCE;
	}

}
