package com.unli.project.exception;

import org.springframework.http.HttpStatus;

import com.unli.project.dto.io.CommonResponse;

@SuppressWarnings("serial")
public class DuplicateUsernameException extends BaseException {
	
	private static final HttpStatus STATUS = HttpStatus.UNPROCESSABLE_ENTITY;
	private static final String MESSAGE = "존재하는 유저네임 입니다.";
	private static final CommonResponse<Void> RESPONSE = new CommonResponse<>(String.valueOf(STATUS.value()), MESSAGE, null);
	private static final DuplicateUsernameException INSTANCE = new DuplicateUsernameException();
	private DuplicateUsernameException() {}
	
	@Override
	public HttpStatus getStatus() {
		return STATUS;
	}
	@Override
	public String getErrorCode() {
		return String.valueOf(STATUS.value());
	}
	@Override
	public String getErrorMessage() {
		return MESSAGE;
	}
	@Override
	public CommonResponse<Void> getErrorResponse() {
		return RESPONSE;
	}
	public static BaseException getInstance() {
		return INSTANCE;
	}

}
