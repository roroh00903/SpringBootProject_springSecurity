package com.unli.project.exception;

import org.springframework.http.HttpStatus;

import com.unli.project.dto.io.CommonResponse;

@SuppressWarnings("serial")
public class PasswordNotMatchedException extends BaseException {
	
	private static final HttpStatus STATUS = HttpStatus.UNAUTHORIZED;
	private static final String MESSAGE = "비밀번호가 일치하지 않습니다.";
	private static final CommonResponse<Void> RESPONSE = new CommonResponse<>(String.valueOf(STATUS.value()), MESSAGE, null);
	private static final PasswordNotMatchedException INSTANCE = new PasswordNotMatchedException();
	private PasswordNotMatchedException() {}
	
	@Override
	public HttpStatus getStatus() {
		return STATUS;
	}
	@Override
	public String getErrorCode() {
		return String.valueOf(STATUS.value());
	}
	@Override
	public String getErrorMessage() {
		return MESSAGE;
	}
	@Override
	public CommonResponse<Void> getErrorResponse() {
		return RESPONSE;
	}
	public static BaseException getInstance() {
		return INSTANCE;
	}

}
