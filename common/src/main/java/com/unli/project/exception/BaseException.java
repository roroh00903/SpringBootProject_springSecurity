package com.unli.project.exception;

import org.springframework.http.HttpStatus;

import com.unli.project.dto.io.CommonResponse;

@SuppressWarnings("serial")
public abstract class BaseException extends RuntimeException{
	
	public abstract HttpStatus getStatus();
	public abstract String getErrorCode();
	public abstract String getErrorMessage();
	public abstract CommonResponse<Void> getErrorResponse();
	
	@Override
	public String getMessage() {
		return getErrorMessage();
	}
	
	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}

}
