package com.unli.project.exception;

import org.springframework.http.HttpStatus;

import com.unli.project.dto.io.CommonResponse;

@SuppressWarnings("serial")
public class PermissionException extends BaseException {
	
	private static final HttpStatus STATUS = HttpStatus.FORBIDDEN;
	private static final String MESSAGE = "접근 권한이 없습니다.";
	private static final CommonResponse<Void> RESPONSE = new CommonResponse<>(String.valueOf(STATUS.value()), MESSAGE, null);
	private static final PermissionException INSTANCE = new PermissionException();
	private PermissionException() {}
	
	@Override
	public HttpStatus getStatus() {
		return STATUS;
	}
	@Override
	public String getErrorCode() {
		return String.valueOf(STATUS.value());
	}
	@Override
	public String getErrorMessage() {
		return MESSAGE;
	}
	@Override
	public CommonResponse<Void> getErrorResponse() {
		return RESPONSE;
	}
	public static BaseException getInstance() {
		return INSTANCE;
	}

}
