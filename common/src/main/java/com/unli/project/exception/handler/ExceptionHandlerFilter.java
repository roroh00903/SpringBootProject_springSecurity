package com.unli.project.exception.handler;

import java.io.IOException;
import java.io.Writer;

import javax.security.sasl.AuthenticationException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unli.project.dto.io.CommonResponse;
import com.unli.project.exception.BaseException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExceptionHandlerFilter extends OncePerRequestFilter {
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		try {
			filterChain.doFilter(request, response);
		} catch (BaseException e) {
			if(HttpStatus.INTERNAL_SERVER_ERROR.equals(e.getStatus()))
				log.error("ASSERTION FAILED >>>>> ", e);
			writeErrorResponse(response, e.getStatus(), e.getErrorResponse());
		} catch (AuthenticationException e) {
			writeErrorResponse(response, HttpStatus.SERVICE_UNAVAILABLE, new CommonResponse<Void>("401", e.getMessage(), null));
		} catch (IllegalArgumentException e) {
			writeErrorResponse(response, HttpStatus.BAD_REQUEST, new CommonResponse<Void>("400", e.getMessage(), null));
		} catch (Exception e) {
			log.error("UNEXPECTED SERVER EXCEPTION >>>>> ", e);
			writeErrorResponse(response, HttpStatus.INTERNAL_SERVER_ERROR, new CommonResponse<Void>("500", "서버 오류가 발생했습니다 (" + e.getMessage() + ")", null));
		}
	}
	
	@SuppressWarnings("deprecation")
	private void writeErrorResponse(HttpServletResponse response, HttpStatus status, Object body) throws ServletException, IOException {
		response.setStatus(status.value());
		response.setContentType("application/json");
		// does not parse utf8 without this
		response.addHeader("content-type", MediaType.APPLICATION_JSON_UTF8_VALUE);
		if(body != null) {
			ObjectMapper mapper = new ObjectMapper();
			String json = mapper.writeValueAsString(body);
			Writer writer = response.getWriter();
			writer.write(json);
			writer.flush();
		}
	}

}

