package com.unli.project.exception;

import org.springframework.http.HttpStatus;

import com.unli.project.dto.io.CommonResponse;

@SuppressWarnings("serial")
public class InvalidParameterException extends BaseException {
	
	private static final HttpStatus STATUS = HttpStatus.UNPROCESSABLE_ENTITY;
	private static final String MESSAGE = "필수 요청 값이 입력되지 않았거나 요청 값이 형식에 어긋납니다.";
	private static final CommonResponse<Void> RESPONSE = new CommonResponse<>(String.valueOf(STATUS.value()), MESSAGE, null);
	private static final InvalidParameterException INSTANCE = new InvalidParameterException();
	private InvalidParameterException() {}
	
	@Override
	public HttpStatus getStatus() {
		return STATUS;
	}
	@Override
	public String getErrorCode() {
		return String.valueOf(STATUS.value());
	}
	@Override
	public String getErrorMessage() {
		return MESSAGE;
	}
	@Override
	public CommonResponse<Void> getErrorResponse() {
		return RESPONSE;
	}
	public static BaseException getInstance() {
		return INSTANCE;
	}

}
