package com.unli.project.exception.handler;

import javax.security.sasl.AuthenticationException;

import org.apache.catalina.connector.ClientAbortException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.firewall.RequestRejectedException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.unli.project.dto.io.CommonResponse;
import com.unli.project.exception.BaseException;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Slf4j
public class ExceptionRestController {
	

	/** unexpected exception */
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public CommonResponse<Void> unexpectedException(Exception e) {
		log.error("UNEXPECTED SERVER EXCEPTION >>>>> ", e);
		return new CommonResponse<Void>("500", "서버 오류가 발생했습니다 (" + e.getMessage() + ")", null);
	}
	
	/** pipe exception */
	@ExceptionHandler(ClientAbortException.class)
	@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
	public void borkenPipeException(ClientAbortException e) {
		// client aborted request... do nothing
		return;
	}
	
	/** authentication exception */
	@ExceptionHandler(AuthenticationException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public CommonResponse<Void> authenticationException(AuthenticationException e) {
		return new CommonResponse<Void>("401", e.getMessage(), null);
	}
	
	/** bad request exception */
	@ExceptionHandler({
				BindException.class, MethodArgumentNotValidException.class, HttpMediaTypeNotSupportedException.class,
				MissingServletRequestPartException.class, MissingServletRequestParameterException.class,
				ConstraintViolationException.class, TypeMismatchException.class, MethodArgumentTypeMismatchException.class,
				MultipartException.class, IllegalArgumentException.class
			})
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public CommonResponse<Void> badRequestException(Exception e) {
		return new CommonResponse<Void>("400", e.getMessage(), null);
	}
	
	/** method not allowed exception */
	@ExceptionHandler({HttpRequestMethodNotSupportedException.class, RequestRejectedException.class})
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	public CommonResponse<Void> methodNotAllowedException(Exception e) {
		return new CommonResponse<Void>("405", e.getMessage(), null);
	}
	
	/** page not found response */
	private final CommonResponse<Void> PAGE_NOT_FOUND_RESPONSE = new CommonResponse<Void>("404", "페이지를 찾을 수 없었습니다.", null);
	/** page not found exception */
	@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public CommonResponse<Void> pageNotFoundException(NoHandlerFoundException e) {
		return PAGE_NOT_FOUND_RESPONSE;
	}
	
	
	
	/** intentional exception */
	@ExceptionHandler(BaseException.class)
	public ResponseEntity<CommonResponse<Void>> intentionalException(BaseException e) {
		if(HttpStatus.INTERNAL_SERVER_ERROR.equals(e.getStatus()))
			log.error("ASSERTION FAILED >>>>> ", e);
		return new ResponseEntity<>(e.getErrorResponse(), e.getStatus());
	}
}

