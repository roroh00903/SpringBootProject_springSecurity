package com.unli.project.util;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import com.unli.project.constant.Constant;
import com.unli.project.exception.ExceedsStringLengthLimitException;
import com.unli.project.exception.InvalidEmailFormException;
import com.unli.project.exception.InvalidParameterException;
import com.unli.project.exception.MissingParameterException;


public class StringUtil {

	public static Integer parseIntCatchException(String value) {
		if (isNullOrEmpty(value))
			return null;
		try {
			return Integer.parseInt(value);
		} catch (NumberFormatException e) {
			return null;
		}
	}

	public static LocalDate parseDateCatchException(String value) {
		if (isNullOrEmpty(value))
			return null;
		try {
			return LocalDate.parse(value);
		} catch (DateTimeParseException e) {
			return null;
		}
	}

	public static LocalDateTime parseDateTime(String value) {
		if (isNullOrEmpty(value))
			return null;
		try {
			return LocalDateTime.parse(value);
		} catch (DateTimeParseException e) {
			throw InvalidParameterException.getInstance();
		}
	}

	public static LocalDate parseDate(String value) {
		if (isNullOrEmpty(value))
			return null;
		try {
			return LocalDate.parse(value);
		} catch (DateTimeParseException e) {
			throw InvalidParameterException.getInstance();
		}
	}

	public static LocalDateTime parseDateTimeCatchException(String value) {
		if (isNullOrEmpty(value))
			return null;
		try {
			return LocalDateTime.parse(value);
		} catch (DateTimeParseException e) {
			return null;
		}
	}

	public static String formatDate(LocalDate value) {
		if (value == null)
			return null;
		return value.format(DateTimeFormatter.ISO_LOCAL_DATE);
	}

	private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	public static String formatDateTime(LocalDateTime value) {
		if (value == null)
			return null;
		return value.format(DATE_TIME_FORMAT);
	}
	
	public static String formatDuration(LocalDateTime value) {
		if (value == null)
			return null;
		
		long time = Duration.between(LocalDateTime.now(), value).toHours()%24;
		long days = Duration.between(LocalDateTime.now(), value).toHours()/24;
		if(time < 0 || days < 0) return null;
		else return days + "일 "+ time +"시간";
	}

	/** 값이 없을 경우 빈 스트링 대신 Null로 치환하여 반환 */
	public static String convertEmptyStringToNull(String value) {
		if (isNullOrEmpty(value))
			return null;
		else
			return value;
	}

	/** String이 db에 varchar(255) 타입으로 저장하기 안전한지 체크 */
	public static boolean varcharTypeSafe(String target) {
		return stringTypeSafeUtil(target, 255);
	}

	/** String이 db에 TEXT 타입으로 저장하기 안전한지 체크 */
	public static boolean textTypeSafe(String target) {
		return stringTypeSafeUtil(target, 4000);
	}

	private static boolean stringTypeSafeUtil(String target, int length) {
		boolean safeToUse = isNullOrEmpty(target) || target.length() <= length;
		if (safeToUse)
			return true;
		else
			throw ExceedsStringLengthLimitException.getInstance();
	}

	/** 값 존재 여부 확인 */
	public static boolean hasValue(String target) {
		return !isNullOrEmpty(target, false);
	}

	/** 값 존재 여부 확인 */
	public static boolean hasValue(String target, boolean throwException) {
		return !isNullOrEmpty(target, throwException);
	}

	/** 값이 비어있는지 확인 */
	public static boolean isNullOrEmpty(String target) {
		return isNullOrEmpty(target, false);
	}

	/** 값이 비어있는지 확인 */
	public static boolean isNullOrEmpty(String target, boolean throwException) {
		if (target == null || target.trim().isEmpty()) {
			if (throwException)
				throw MissingParameterException.getInstance();
			else
				return true;
		} else {
			return false;
		}
	}

	/** 휴대번호 균일화 */
	public static String uniformizePhoneNumber(String target) {
		if (!isPhoneNumber(target, false))
			return null;
		return target.trim().replaceAll("[^\\d]", "");
	}

	/** 전화번호인지 체크 */
	public static boolean isPhoneNumber(String target, boolean throwException) {
		if (isNullOrEmpty(target, throwException))
			return false;

		if (target.matches("^\\d{2,3}-?\\d{3,4}-?\\d{4}$")) {
			return true;
		} else {
			if (throwException)
				throw InvalidParameterException.getInstance();
			else
				return false;
		}
	}

	/** 날짜인지 체크 */
	public static boolean isDate(String target, boolean throwException) {
		if (isNullOrEmpty(target, throwException))
			return false;

		try {
			LocalDate.parse(target);
			return true;
		} catch (DateTimeParseException e) {
			if (throwException)
				throw InvalidParameterException.getInstance();
			else
				return false;
		}
	}

	/** 날짜인지 체크 */
	public static boolean isDateTime(String target, boolean throwException) {
		if (isNullOrEmpty(target, throwException))
			return false;

		try {
			LocalDateTime.parse(target);
			return true;
		} catch (DateTimeParseException e) {
			if (throwException)
				throw InvalidParameterException.getInstance();
			else
				return false;
		}
	}
	/**	비밀번호 유효성 체크 (영문 숫자 특수문자 조합 8자리 이상)**/
	public static boolean isVaildPassword(String password) {
		boolean chk = false;
		String reg = "^(?=.*[A-Za-z])(?=.*[0-9])(?=.*[!@#$%^&*?,./\\\\<>|_-[+]=\\`~\\(\\)\\[\\]\\{\\}])[A-Za-z[0-9]!@#$%^&*?,./\\\\<>|_-[+]=\\`~\\(\\)\\[\\]\\{\\}]{8,20}$"; // 영문, 숫자, 특수문자
		Matcher match;
		 match = Pattern.compile(reg).matcher(password);

		  if(match.find()) {
		   chk = true;
		  }
		  return chk;
	}
	
	/** 이메일 유효성 체크 */
	public static boolean isValidEmail(String email) {
		boolean err = false;
		String regex = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(email);
		if (m.matches()) {
			err = true;
		} else throw InvalidEmailFormException.getInstance();
		return err;
	}

	/** ip 값 찾기 */
	public static String getIp(HttpServletRequest request) {
		String ip = request.getHeader("X-FORWARDED-FOR");
		if (ip == null)
			ip = request.getHeader("Proxy-Client-IP");
		if (ip == null)
			ip = request.getHeader("WL-Proxy-Client-IP");
		if (ip == null)
			ip = request.getHeader("HTTP_CLIENT_IP");
		if (ip == null)
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		if (ip == null)
			ip = request.getRemoteAddr();
		if ("0:0:0:0:0:0:0:1".equals(ip) || "127.0.0.1".equals(ip))
			ip = "localhost";
		return ip;
	}

	/** device 값 찾기 */
	public static String getDevice(HttpServletRequest request) {
		String device = request.getHeader(Constant.DEVICE_HEADER_NAME);
		return device;
	}

	/** 파일 url 생성 */
	public static String generateFileUrl() {
		LocalDate now = LocalDate.now();
		return String.format("%s%02d%02d%02d", generateRandomString(18), (now.getYear() % 100), now.getMonthValue(),
				now.getDayOfMonth());
	}

	private static final String RANDOM_STRING_POOL = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

	/** 랜덤한 코드값 생성 */
	public static String generateRandomString(int length) {
		StringBuilder value = new StringBuilder("");
		if (length > 0) {
			Random random = new Random();
			for (int i = 0; i < length; i++) {
				value.append(RANDOM_STRING_POOL.charAt(random.nextInt(RANDOM_STRING_POOL.length())));
			}
		}
		return value.toString();
	}
}
