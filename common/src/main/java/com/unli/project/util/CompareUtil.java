package com.unli.project.util;

import com.unli.project.domain.Admin;
import com.unli.project.domain.User;
import com.unli.project.exception.PermissionException;

public class CompareUtil {

	public static void compare(User user, User user2) {
		if(user ==  null || user2 == null ) throw PermissionException.getInstance();
		if(!user.getId().equals(user2.getId())) throw PermissionException.getInstance();
	}
	
	public static void compare(Admin admin, Admin admin2) {
		if(admin ==  null || admin2 == null ) throw PermissionException.getInstance();
		if(!admin.getId().equals(admin2.getId())) throw PermissionException.getInstance();
	}
}
