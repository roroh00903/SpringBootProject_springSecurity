package com.unli.project.util;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unli.project.exception.InvalidParameterException;
import com.unli.project.exception.TargetNotFoundException;

public class FindByIdUtil {
	public static <T> T findTarget(JpaRepository<T, Long> repo, Long id) {
		if(id == null) return null;
		return repo.findById(id).orElse(null);
	}
	
	public static <T> T findTargetOrThrow(JpaRepository<T, Long> repo, Long id) {
		return findTargetOrThrow(repo, id, false);
	}
	
	public static <T> T findTargetOrThrow(JpaRepository<T, Long> repo, Long id, boolean ignoreInvalidated) {
		if(id == null) throw InvalidParameterException.getInstance();
		T t = repo.findById(id).orElseThrow(()->TargetNotFoundException.getInstance());
		return t;
	}
}

