package com.unli.project.advise;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.unli.project.util.StringUtil;

/** interceptor to handle commonly included parameter */
public class ParameterInterceptor extends HandlerInterceptorAdapter {
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		@SuppressWarnings("unchecked")
		Map<String, String> pathVariables =
			(Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
		
		return true;
	}
	
	private Long parseId(String idString) {
		if(StringUtil.isNullOrEmpty(idString)) return null;
		
		try {
			return Long.parseLong(idString);
		} catch (NumberFormatException e) {
			// id is not number... do nothing
		}
		
		return null;
	}
}
