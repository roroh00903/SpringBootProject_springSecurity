package com.unli.project.domain;

import javax.persistence.Entity;

import com.unli.project.domain.base.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class FileData extends BaseEntity {

	private String fileName;
	private String fileOriginalName;
	private String fileUrl;
	private boolean deleted;

}
