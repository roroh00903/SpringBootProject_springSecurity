package com.unli.project.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.unli.project.domain.base.UpdatableEntity;
import com.unli.project.type.LoginFailureReason;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class AdminLoginHistory extends UpdatableEntity {
	@ManyToOne(fetch = FetchType.EAGER)
	private Admin admin;
	private String ip;
	private String device;
	private boolean success;
	@Enumerated(EnumType.STRING)
	private LoginFailureReason failureReason;
}
