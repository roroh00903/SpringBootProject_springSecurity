package com.unli.project.domain.base;

import java.time.LocalDateTime;

import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
public abstract class UpdatableEntity extends BaseEntity {
	@UpdateTimestamp
	private LocalDateTime updatedAt;
}
