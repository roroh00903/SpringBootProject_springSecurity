package com.unli.project.domain;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.unli.project.domain.base.InvalidatableEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/** 자동 로그인용 접근 토큰 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class AdminAccessToken extends InvalidatableEntity {
	@ManyToOne(fetch = FetchType.EAGER)
	private Admin admin;
	private String token;
	/** 만료 예정일 */
	private LocalDateTime expireAt;
}
