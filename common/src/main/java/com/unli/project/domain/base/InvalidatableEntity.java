package com.unli.project.domain.base;

import java.time.LocalDateTime;

import javax.persistence.MappedSuperclass;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
public abstract class InvalidatableEntity extends UpdatableEntity {
	private boolean invalid;
	private LocalDateTime invalidAt;
	
	public void setInvalid(boolean invalid) {
		this.invalid = invalid;
		if(invalid) {
			invalidAt = LocalDateTime.now();
		} else {
			invalidAt = null;
		}
	}
}
