package com.unli.project.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

import com.unli.project.domain.base.InvalidatableEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User extends InvalidatableEntity {
	@Column(unique = true)
	private String username;
	private String password;
	private String name;
	private String email;
	private LocalDateTime lastLoginTime;
	boolean temporaryPassword;
	private int loginFailCount;

	@Transient
	public boolean isAccountLocked() {
		return loginFailCount >= 5;
	}

}
