package com.unli.project.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.unli.project.domain.base.InvalidatableEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Admin extends InvalidatableEntity {
	@Column(unique = true)
	private String username;
	private String password;
	private String name;
	boolean masterAdmin;

}
