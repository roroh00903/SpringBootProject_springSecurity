package com.unli.project.type;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public enum LoginFailureReason {
	/** 아이디/비번 오류 */
	AUTHENTICATION_FAIL("아이디 또는 비밀번호가 틀렸습니다."),
	/** 오류 회수 초과 */
	ATTEMPT_EXCEED_LIMIT("로그인 시도 횟수를 초과하였습니다."),
	/** 알 수 없는 오류 */
	UNKNOWN_EXCEPTION("로그인에 실패했습니다.")
	;
	private String message;
	public String getMessage() {return message;}
	
	private LoginFailureReason(String message) {this.message = message;}
	
	public static LoginFailureReason by(AuthenticationException exception) {
		if(exception == null) return AUTHENTICATION_FAIL;
		
		if(exception instanceof BadCredentialsException || exception instanceof UsernameNotFoundException) {
			return AUTHENTICATION_FAIL;
		} else if(exception instanceof LockedException) {
			return ATTEMPT_EXCEED_LIMIT;
		} else {
			return UNKNOWN_EXCEPTION;
		}
	}

}
