package com.unli.project.constant;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;

public interface Constant {
	
	/* ------------------------------------------ Values ------------------------------------------ */
	
	public static final int MEMBER_CODE_LENGTH = 7;
	public static final int ADMIN_CODE_LENGTH = 7;
	
	public static final int EVALUATION_DUE = 30; // in days
	public static final int MEMBER_PUSH_TOKEN_EXPIRE = 365; // in days
	
	public static final String COMMON_PATH = "/c";
	
	/* ------------------------------------------ Access ------------------------------------------ */
	
	// admin
	public static final int ADMIN_LOGIN_ATTEMPT_MAXIMUM = 5;
	public static final int ADMIN_LOGIN_TOKEN_LIFESPAN = 30; // in days
	
	// user
	public static final int USER_LOGIN_ATTEMPT_MAXIMUM = 5;
	public static final int USER_LOGIN_TOKEN_LIFESPAN = 30; // in days
	
	/* ------------------------------------------ Headers ------------------------------------------ */
	
	public static final String ADMIN_LOGIN_TOKEN_HEADER_NAME = "UNLI-ADMINTOKEN";
	public static final String USER_LOGIN_TOKEN_HEADER_NAME = "UNLI-USERTOKEN";
	
	public static final String ADMIN_CSRF_HEADER_NAME = "UNLI-HEADER-CSRF";
	public static final String ADMIN_CSRF_COOKIE_NAME = "UNLI-COOKIE-CSRF";
	
	public static final String DEVICE_HEADER_NAME = "UNLI-DEVICE";
	
	public static final String ADMIN_CORS_ALLOWED_ORIGIN = "*";
	
	/* ------------------------------------------ Configs ------------------------------------------ */
	
	public static final String[] RESOURCE_URI = {"/webjars/**", "/css/**", "/img/**", "/js/**"};
	
	public static final int DB_VARCHAR_TYPE_LENGTH = 255;
	public static final int DB_TEXT_TYPE_LENGTH = 4000;
	
	/* ------------------------------------------ Object ------------------------------------------ */
	
	
	
	/* ------------------------------------------ Exception ------------------------------------------ */
	/** 아이디/비번 오류 */
	public static final BadCredentialsException AUTH_FAIL_EXCEPTION = new BadCredentialsException("계정 인증에 실패했습니다.");
	/** 아이디/비번 오류 */
	public static final BadCredentialsException AUTH_FAIL_PASSWORD_ISSUED_EXCEPTION = new BadCredentialsException("유효하지 않는 임시 비밀번호 입니다.");
	/** 계정 잠금 오류 */
	public static final LockedException ACCOUNT_LOCKED_EXCEPTION = new LockedException("접속 시도 제한을 초과로 임시비밀번호가 발급되었습니다.");
	/** 계정 만료 오류 */
	public static final DisabledException ACCOUNT_DISABLED_EXCEPTION = new DisabledException("만료된 계정입니다.");
}

