package com.unli.project.dto;

import com.unli.project.domain.FileData;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FileDataDto {
	private Long id;
	
	/** for service level logic purpose */
//	@JsonIgnore
//	private Field field;
//	@JsonIgnore
//	private CobaTest cobaTest;
//	@JsonIgnore
//	private Question question;
	
	private String fileOrignalName;
	private String fileName;
	private String fileUrl;
	
	public FileDataDto(FileData data) {
		id = data.getId();
		fileOrignalName = data.getFileOriginalName();
		fileName = data.getFileName();
		fileUrl = data.getFileUrl();
	}
}

