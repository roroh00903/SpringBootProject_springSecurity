package com.unli.project.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.unli.project.domain.User;
import com.unli.project.util.StringUtil;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDto {
	private Long id;
	private String username;
	private String name;
	private String email;
	private String createAt;
	private String lastLoginTime;
	
	@JsonIgnore
	private String password;
	@JsonIgnore
	private String oldPassword;

	public UserDto(User user) {
		this.id = user.getId();
		this.username = user.getUsername();
		this.name = user.getName();
		this.email = user.getEmail();
		this.createAt = StringUtil.formatDateTime(user.getCreatedAt());
		this.lastLoginTime = StringUtil.formatDateTime(user.getLastLoginTime());
	}


}
