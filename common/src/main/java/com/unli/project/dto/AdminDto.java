package com.unli.project.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.unli.project.domain.Admin;
import com.unli.project.util.StringUtil;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AdminDto {
	private Long id;
	private String username;
	private String name;
	private String createdAt;
	private boolean masterAdmin;
	
	@JsonIgnore
	private String password;
	@JsonIgnore
	private String oldPassword;

	public AdminDto(Admin admin) {
		this.id = admin.getId();
		this.username = admin.getUsername();
		this.name = admin.getName();
		this.createdAt = StringUtil.formatDateTime(admin.getCreatedAt());
		this.masterAdmin = admin.isMasterAdmin();
	}


}
