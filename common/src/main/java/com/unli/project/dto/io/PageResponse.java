package com.unli.project.dto.io;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageResponse<T> {
	private String responseCode;
	private String message;
	private List<T> data;
	private int currentIndex;
	private long totalElement;
	private long currentPage;
	private long totalPage;
	
	public PageResponse(Page<T> data, Pageable page) {
		this("200", "SUCCESS", data, page);
	}
	
	public PageResponse(String responseCode, String message, Page<T> data, Pageable page) {
		this.responseCode = responseCode;
		this.message = message;
		setData(data, page);
	}
	
	public void setData(Page<T> data, Pageable page) {
		this.data = data.getContent();
		this.currentIndex = data.getNumber();
		this.currentPage = page.getPageNumber();
		this.totalPage = data.getTotalElements() / page.getPageSize();
		if(data.getTotalElements() % page.getPageSize() > 0) totalPage++;
		this.totalElement = data.getTotalElements();
	}
	
	public static class Builder<T> {
		private String responseCode = "200";
		private String message = "SUCCESS";
		private List<T> data;
		private int currentIndex;
		private long currentPage;
		private long totalPage;
		private long totalElement;
		
		public Builder<T> responseCode(String responseCode) {
			this.responseCode = responseCode;
			return this;
		}
		
		public Builder<T> message(String message) {
			this.message = message;
			return this;
		}
		
		public Builder<T> data(List<T> data) {
			this.data = data;
			return this;
		}
		
		public Builder<T> data(Page<T> data, Pageable page) {
			this.data = data.getContent();
			this.currentIndex = data.getNumber();
			this.currentPage = page.getPageNumber();
			this.totalPage = data.getTotalElements() / page.getPageSize();
			if(data.getTotalElements() % page.getPageSize() > 0) totalPage++;
			this.totalElement = data.getTotalElements();
			return this;
		}
		
		public PageResponse<T> build() {
			return new PageResponse<>(responseCode, message, data, currentIndex, currentPage, totalPage, totalElement);
		}
	}
}
