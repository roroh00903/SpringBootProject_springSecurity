package com.unli.project.dto.io;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommonResponse<T> {
	/** 빈 응답 객체 */
	public static final CommonResponse<Void> OK = new CommonResponse<Void>("200", "SUCCESS", null);
	
	private String responseCode;
	private String message;
	private T data;
	
	public CommonResponse(T data) {
		this.responseCode = "200";
		this.message = "SUCCESS";
		this.data = data;
	}
	
	public static class Builder<T> {
		private String responseCode = "200";
		private String message = "SUCCESS";
		private T data;
		
		public Builder<T> responseCode(String responseCode) {
			this.responseCode = responseCode;
			return this;
		}
		
		public Builder<T> message(String message) {
			this.message = message;
			return this;
		}
		
		public Builder<T> data(T data) {
			this.data = data;
			return this;
		}
		
		public CommonResponse<T> build() {
			return new CommonResponse<>(responseCode, message, data);
		}
	}
}
