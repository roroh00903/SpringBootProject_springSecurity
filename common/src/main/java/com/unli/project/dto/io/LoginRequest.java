package com.unli.project.dto.io;

import lombok.Data;

@Data
public class LoginRequest {
	String username;
	String password;
}
