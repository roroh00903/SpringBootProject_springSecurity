package com.unli.project.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.unli.project.domain.Admin;
import com.unli.project.dto.AdminDto;

public interface AdminRepository extends JpaRepository<Admin, Long>  {

	public Admin findByUsernameAndInvalidFalse(String username);

	public Admin findByUsername(String username);

	public Page<AdminDto> findAllByInvalidFalse(Pageable pageable);

}
