package com.unli.project.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unli.project.domain.Admin;
import com.unli.project.domain.AdminAccessToken;

public interface AdminAccessTokenRepository extends JpaRepository<AdminAccessToken, Long> {

	List<AdminAccessToken> findWithAdminByTokenAndInvalidFalse(String token);

	List<AdminAccessToken> findByTokenAndInvalidFalse(String tokenString);

	List<AdminAccessToken> findByAdminAndInvalidFalse(Admin user);

	List<AdminAccessToken> findByAdminAndInvalidFalseOrderByIdDesc(Admin admin);

	List<AdminAccessToken> findByToken(String token);

	List<AdminAccessToken> findByTokenAndAdmin(String header, Admin admin);

}
