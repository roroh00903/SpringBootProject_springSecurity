package com.unli.project.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unli.project.domain.User;
import com.unli.project.domain.UserAccessToken;

public interface UserAccessTokenRepository extends JpaRepository<UserAccessToken, Long> {

	List<UserAccessToken> findWithUserByTokenAndInvalidFalse(String token);

	List<UserAccessToken> findByTokenAndInvalidFalse(String tokenString);

	List<UserAccessToken> findByUserAndInvalidFalse(User user);

	List<UserAccessToken> findByToken(String token);

	List<UserAccessToken> findByTokenAndUser(String header, User user);

}
