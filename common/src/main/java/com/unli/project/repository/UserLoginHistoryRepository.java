package com.unli.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unli.project.domain.UserLoginHistory;

public interface UserLoginHistoryRepository extends JpaRepository<UserLoginHistory, Long> {

}
