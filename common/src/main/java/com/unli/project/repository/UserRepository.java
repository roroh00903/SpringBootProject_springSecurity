package com.unli.project.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.unli.project.domain.User;
import com.unli.project.dto.UserDto;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByUsernameAndInvalidFalse(String username);

	User findByEmailAndInvalidFalse(String email);

	User findByUsername(String username);

	Page<UserDto> findAllByInvalidFalse(Pageable pageable);

}
