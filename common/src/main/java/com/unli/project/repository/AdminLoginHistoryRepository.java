package com.unli.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unli.project.domain.AdminLoginHistory;

public interface AdminLoginHistoryRepository extends JpaRepository<AdminLoginHistory, Long> {

}
