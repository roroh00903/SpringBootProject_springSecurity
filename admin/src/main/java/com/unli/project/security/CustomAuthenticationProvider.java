package com.unli.project.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.unli.project.constant.Constant;
import com.unli.project.util.StringUtil;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
	@Autowired
	private CustomUserDetailService userDetailService;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {
		// user info
		String username = (String)auth.getPrincipal();
		String password = (String)auth.getCredentials();
		
		if(username == null) username = "";
		if(password == null) password = "";
		
		AdminAuthentication userAuth = null;
		
		// load authentication
		if(StringUtil.hasValue(username)) userAuth = userDetailService.loadUserByUsername(username);
		else if(auth instanceof AdminAuthentication) userAuth = (AdminAuthentication) auth;
		else throw Constant.AUTH_FAIL_EXCEPTION;
		
		// exceed login attempt limit
		// invalid account
		if(!userAuth.isEnabled()) {
			throw Constant.ACCOUNT_DISABLED_EXCEPTION;
		}
		// authentication
		boolean authenticated = userAuth.isAuthenticated() && userAuth.getAdmin() != null;
		if(authenticated) {
			// already authenticated by access token
			setAuthentication(userAuth);
			return userAuth;
		} else {
			// password check
			boolean passwordMatch = password != null && passwordEncoder.matches(password, userAuth.getPassword());
			if(passwordMatch) {
				// login success
				setAuthentication(userAuth);
				return userAuth;
			} else {
				// login failure
				throw Constant.AUTH_FAIL_EXCEPTION;
			}
		}
	}
	
	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class) || authentication.equals(AdminAuthentication.class);
	}
	
	private void setAuthentication(AdminAuthentication auth) {
		SecurityContextHolder.getContext().setAuthentication(auth);
	}
}
