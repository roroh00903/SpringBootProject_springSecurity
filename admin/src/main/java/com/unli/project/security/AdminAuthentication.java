package com.unli.project.security;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.unli.project.domain.Admin;

import lombok.Data;

@Data
public class AdminAuthentication implements UserDetails, Authentication {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5348707589824568596L;

	/** 인증 확인 여부 */
	private boolean authenticated;
	/** 유저 정보 */
	private Admin admin;
	
	/** 인증 정보 - 아이디 */
	private String principal;
	/** 인증 정보 - 비밀번호 */
	private String credential;
	/** 인증 정보 - 토큰 */
	private String authToken;
	
	/** 권한 */
	private boolean masterAdmin;
	
	/* ---------------------------------- AdminAuthentication ---------------------------------- */
	
	/** authenticate with id/password */
	public AdminAuthentication(String principal, String credential) {
		this.principal = principal;
		this.credential = credential;
	}
	
	/** authenticate with user info */
	public AdminAuthentication(Admin user, boolean authenticated) {
		if(user != null) {
			setAuthenticated(authenticated);
			setUser(user);
			this.masterAdmin = user.isMasterAdmin();
		}
	}
	
	public void setUser(Admin user) {
		if(user != null) {
			this.admin = user;
		}
	}
	
	/* ---------------------------------- UserDetails Overrides ---------------------------------- */
	
	@Override
	public String getPassword() {
		return admin.getPassword();
	}
	
	@Override
	public String getUsername() {
		return admin.getUsername();
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return !admin.isInvalid();
	}
	
	@Override
	public boolean isAccountNonLocked() {
		return !admin.isInvalid();
	}
	
	@Override
	public boolean isCredentialsNonExpired() {
		return !admin.isInvalid();
	}
	
	@Override
	public boolean isEnabled() {
		return !admin.isInvalid();
	}
	
	/* ---------------------------------- Authentication Overrides ---------------------------------- */

	@Override
	public String getName() {
		if(admin != null) return admin.getName();
		else return null;
	}
	
	@Override
	public String getCredentials() {
		return this.credential;
	}
	
	@Override
	public AdminAuthentication getDetails() {
		return this;
	}
	
	@Override
	public String getPrincipal() {
		return this.principal;
	}
	
	@Override
	public boolean isAuthenticated() {
		return this.authenticated;
	}
	
	@Override
	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
		this.authenticated = isAuthenticated;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}
}
