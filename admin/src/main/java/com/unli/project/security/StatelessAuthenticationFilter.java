package com.unli.project.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.filter.GenericFilterBean;

import com.unli.project.constant.Constant;
import com.unli.project.domain.Admin;
import com.unli.project.service.AdminLoginService;
import com.unli.project.util.StringUtil;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class StatelessAuthenticationFilter extends GenericFilterBean {
	
	private AdminLoginService loginService;
	private CustomAuthenticationProvider authProvider;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// get token info
		String adminToken = ((HttpServletRequest) request).getHeader(Constant.ADMIN_LOGIN_TOKEN_HEADER_NAME);
		
		// continue filter chain if token value is empty
		if(StringUtil.isNullOrEmpty(adminToken)) {
			chain.doFilter(request, response);
			return;
		}
		// get user info by token
		Admin admin = loginService.getUserByToken(adminToken, false, true);
		// authenticate
		AdminAuthentication auth = new AdminAuthentication(admin, true);
		auth.setAuthToken(adminToken);
		authProvider.authenticate(auth);
		
		// always continue
		chain.doFilter(request, response);
	}
}
