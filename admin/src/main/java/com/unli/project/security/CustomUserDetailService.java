package com.unli.project.security;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.unli.project.domain.Admin;
import com.unli.project.repository.AdminRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
	
	private AdminRepository adminRepo;
	
	private static final UsernameNotFoundException EXCEPTION = new UsernameNotFoundException("대상 유저를 찾을 수 없습니다.");
	
	@Override
	public AdminAuthentication loadUserByUsername(String username) throws UsernameNotFoundException {
		Admin admin = adminRepo.findByUsername(username);
		if(admin == null) {
			throw EXCEPTION;
		}
		return new AdminAuthentication(admin, false);
	}
}
