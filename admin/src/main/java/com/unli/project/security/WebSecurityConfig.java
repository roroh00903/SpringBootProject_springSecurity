package com.unli.project.security;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.CorsUtils;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.unli.project.constant.Constant;
import com.unli.project.exception.handler.ExceptionHandlerFilter;
import com.unli.project.service.AdminLoginService;

import lombok.AllArgsConstructor;

@Configuration
@EnableWebSecurity
@AllArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	private CustomAuthenticationProvider authProvider;
	private CustomUserDetailService userDetailService;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	private AdminLoginService loginService;
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring()
			.antMatchers(
					"/v2/api-docs",
					"/swagger-resources",
					"/swagger-resources/configuration/ui",
					"/swagger-ui.html",
	        		"/configuration/security",
	        		"/webjars/**"
			)
		;
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// default csrf disable
		http.csrf().disable()
		
		// session policy
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		
		// allow certain pages
		.and().authorizeRequests()
			
			/* ------ 테스트 중에만 임시적으로 허용할 목록 ------ */
			.antMatchers("/test/**", "/swagger-ui.html")
//			.denyAll()
			.permitAll()
			/* ----------------------------------------- */
			
			/* ------------------ cors ----------------- */
			.requestMatchers(CorsUtils::isPreFlightRequest).permitAll()
			/* ----------------------------------------- */
			
			/* ------------- authentication ------------ */
			.antMatchers(HttpMethod.POST, "/login").permitAll()
			.antMatchers(HttpMethod.POST, "/logout").authenticated()
			/* ----------------------------------------- */
			
			/* ---------------- common info --------------- */
			.antMatchers(Constant.COMMON_PATH + "/**").authenticated()
			/* ----------------------------------------- */
			
			
			
		// cors
		.and()
			.cors()
		// exception handling
		.and().exceptionHandling()
			.accessDeniedHandler(accessDeniedHandler())
			
		// filters
		.and()
			// disable default logout
			.logout().disable()
			// exception hanlder filter
			.addFilterAfter(new ExceptionHandlerFilter(), SecurityContextPersistenceFilter.class)
			// csrf filter
//			.addFilterBefore(new StatelessCsrfFilter(), CsrfFilter.class)
			// auth filter
			.addFilterBefore(new StatelessAuthenticationFilter(loginService, authProvider), UsernamePasswordAuthenticationFilter.class)
		;
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
			.authenticationProvider(authProvider)
			.userDetailsService(userDetailService)
			.passwordEncoder(passwordEncoder);
	}
	
	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.addAllowedOrigin(Constant.ADMIN_CORS_ALLOWED_ORIGIN);
		configuration.addAllowedMethod("*");
		configuration.addAllowedHeader("*");
		configuration.setAllowCredentials(true);
		configuration.setMaxAge(3600L);
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}
	
	@Bean
	@ConditionalOnClass(name = "org.springframework.security.web.access.AccessDeniedHandler")
	public AccessDeniedHandler accessDeniedHandler() {
	    return (request, response, exception) -> {
	        if (!response.isCommitted()) {
	            request.setAttribute("ERROR_ATTRIBUTE", exception);
	            response.sendError(HttpServletResponse.SC_FORBIDDEN);
	        }
	    };
	}
}