package com.unli.project.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;

import com.unli.project.constant.Constant;
import com.unli.project.exception.CsrfException;
import com.unli.project.util.StringUtil;

public class StatelessCsrfFilter extends OncePerRequestFilter {

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		if(request.getRequestURI().equals("/swagger-ui.html")) {
			filterChain.doFilter(request, response);
			return;
		}
		
		// get header and cookie info
		final Cookie[] cookies = request.getCookies();
		final String csrfHeader = request.getHeader(Constant.ADMIN_CSRF_HEADER_NAME);
		String csrfCookie = null;
		
		if(cookies != null) {
			for (Cookie cookie : cookies) {
				if(cookie.getName().equals(Constant.ADMIN_CSRF_COOKIE_NAME)) {
					csrfCookie = cookie.getValue();
				}
			}
		}
		
		// check csrf match
		if(StringUtil.isNullOrEmpty(csrfHeader) || !csrfHeader.equals(csrfCookie)) {
			throw CsrfException.getInstance();
		}
		
		// always continue
		filterChain.doFilter(request, response);
	}
}
