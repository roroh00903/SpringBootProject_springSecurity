package com.unli.project.dto;

import com.unli.project.domain.AdminAccessToken;
import com.unli.project.security.AdminAuthentication;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminAuthForAdminDto {
	private AdminDto admin;
	private String authToken;
	private boolean MasterAdmin;
	
	public AdminAuthForAdminDto(AdminAuthentication auth) {
		if(auth.getAdmin() != null) admin = new AdminDto(auth.getAdmin());
		authToken = auth.getAuthToken();
		MasterAdmin = auth.isMasterAdmin();
	}

	public AdminAuthForAdminDto(AdminAccessToken adminAccessToken) {
		this.admin = new AdminDto(adminAccessToken.getAdmin());
		this.authToken = adminAccessToken.getToken();
		this.MasterAdmin = adminAccessToken.getAdmin().isMasterAdmin();
	}
}
